import React from 'react';

import '../styles/FitnessCardTop.css';

class FitnessCardTop extends React.Component {

	constructor (props) {
		super(props);
	}

	render() {
		return (
			<div className='fitness-card-top'>
				<img className='fitness-card-image' src={this.props.imageSrcPrefix + '-thumb.jpg'} />
				{this.props.children}
			</div>
		);
	}
}

export default FitnessCardTop;
