import React from 'react';

import '../styles/MultiWorkoutImageOverlay.css';

class MultiWorkoutImageOverlay extends React.Component {

	constructor (props) {
		super(props);
	}

	render() {
		return (
	       <div className='multi-workout-image-overlay'>
	       	<div className='num-workouts'>{this.props.numWorkouts}</div>
	       	<div>Workouts</div>
	       	<div><img src='./icons/playlist.svg' /></div>
	       </div>
		);
	}
}

export default MultiWorkoutImageOverlay;
