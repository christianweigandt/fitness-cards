import React from 'react';

import '../styles/FitnessCardBottom.css';

class FitnessCardBottom extends React.Component {

	constructor (props) {
		super(props);
	}

	render() {
		return (
			<div className='fitness-card-bottom'>
				<div className='fitness-card-title-row'>
					<div className='fitness-card-title'>{this.props.cardTitle}</div>
					<img className='fitness-card-trainer-img' src={this.props.imageSrcPrefix + '-trainer.jpg'} />
				</div>
				{this.props.children}
			</div>
		);
	}
}

export default FitnessCardBottom;