import React from 'react';

import '../styles/WorkoutMeasurements.css';

class WorkoutMeasurements extends React.Component {

	constructor (props) {
		super(props);

		// Todo - not perfect alignment for the icons
	}

	render() {
		return (
	       <div className='workout-measurements'>
	       	<div><img src='./icons/timer.svg' /></div>
	       	<div>{this.props.workoutTime}</div>
	       	<div><img src='./icons/distance.svg' /></div>
	       	<div>{this.props.workoutDistance}</div>
	       </div>
		);
	}
}

export default WorkoutMeasurements;
