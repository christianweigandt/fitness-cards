import React from 'react';
import FitnessCardTop from './FitnessCardTop';
import FitnessCardBottom from './FitnessCardBottom';
import WorkoutMeasurements from './WorkoutMeasurements';

import '../styles/FitnessCard.css';

class FitnessCard extends React.Component {

	constructor (props) {
		super(props);
	}

	render() {
		return (
			<div className={(this.props.isActive ? 'active ' : '') + 'fitness-card'} onClick={this.props.clickHandler}>
				<FitnessCardTop imageSrcPrefix={this.props.imageSrcPrefix}>
					{this.props.imageOverlay}
				</FitnessCardTop>
				<FitnessCardBottom imageSrcPrefix={this.props.imageSrcPrefix} cardTitle={this.props.cardTitle}>
					{this.props.workoutTime && <WorkoutMeasurements workoutTime={this.props.workoutTime} workoutDistance={this.props.workoutDistance} />}
					<div className='view-details-link'>VIEW DETAILS</div>
				</FitnessCardBottom>
			</div>
		);
	}
}

export default FitnessCard;
