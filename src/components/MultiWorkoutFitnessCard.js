import React from 'react';
import FitnessCard from './FitnessCard';
import MultiWorkoutImageOverlay from './MultiWorkoutImageOverlay';

class MultiWorkoutFitnessCard extends React.Component {

	constructor (props) {
		super(props);
	}

	render() {
		return (
	       <FitnessCard 
	       		imageSrcPrefix={this.props.imageSrcPrefix}
	       		cardTitle={this.props.cardTitle} 
	       		isActive={this.props.isActive}
	       		clickHandler={this.props.clickHandler}
	       		imageOverlay={
	       			<MultiWorkoutImageOverlay numWorkouts={this.props.numWorkouts} />
	       		}
       		/>
		);
	}
}

export default MultiWorkoutFitnessCard;
