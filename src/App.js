import React from 'react';
import FitnessCard from './components/FitnessCard';
import MultiWorkoutFitnessCard from './components/MultiWorkoutFitnessCard';

import './App.css';

class App extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      activeIndex: -1
    };
  }

  handleClick (index) {
    this.setState({
      activeIndex: index
    });
  }

  render() {
    return (
      <div className='page'>
        <FitnessCard 
          imageSrcPrefix='./images/lake-inniscarra' 
          cardTitle='Lake Inniscarra, Ireland - Pyramid' 
          workoutTime='30:53'
          workoutDistance='6,248 M' 
          isActive={this.state.activeIndex === 0}
          clickHandler={this.handleClick.bind(this, 0)}/>
        <MultiWorkoutFitnessCard 
          imageSrcPrefix='./images/performance-series' 
          cardTitle='Performance Series' 
          numWorkouts='9'  
          isActive={this.state.activeIndex === 1}
          clickHandler={this.handleClick.bind(this, 1)}/>
        <FitnessCard 
          imageSrcPrefix='./images/slow-pulls' 
          cardTitle='Slow Pulls and Fast Intervals' 
          workoutTime='44:13'
          workoutDistance='9,948 M'  
          isActive={this.state.activeIndex === 2}
          clickHandler={this.handleClick.bind(this, 2)}/>
        <MultiWorkoutFitnessCard 
          imageSrcPrefix='./images/20-minutes-to-toned' 
          cardTitle='20 Minutes To Toned' 
          numWorkouts='12'  
          isActive={this.state.activeIndex === 3}
          clickHandler={this.handleClick.bind(this, 3)}/>
        <FitnessCard 
          imageSrcPrefix='./images/charles-race' 
          cardTitle='Charles Race, Boston, Massachusetts' 
          workoutTime='36:22'
          workoutDistance='8,648 M'  
          isActive={this.state.activeIndex === 4}
          clickHandler={this.handleClick.bind(this, 4)}/>
        <MultiWorkoutFitnessCard 
          imageSrcPrefix='./images/full-body-hiit' 
          cardTitle='Full-Body HIIT Series' 
          numWorkouts='12'  
          isActive={this.state.activeIndex === 5}
          clickHandler={this.handleClick.bind(this, 5)}/>
        <FitnessCard 
          imageSrcPrefix='./images/kafue-river' 
          cardTitle='Kafue River, Zambia - Power Stroke Pyramid' 
          workoutTime='22:22'
          workoutDistance='4,660'  
          isActive={this.state.activeIndex === 6}
          clickHandler={this.handleClick.bind(this, 6)}/>
        <MultiWorkoutFitnessCard 
          imageSrcPrefix='./images/shred-and-burn' 
          cardTitle='Shred & Burn Series' 
          numWorkouts='16'  
          isActive={this.state.activeIndex === 7}
          clickHandler={this.handleClick.bind(this, 7)}/>
      </div>
    );
  }
}
export default App;
